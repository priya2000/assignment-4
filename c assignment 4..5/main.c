#include <stdio.h>
#include <stdlib.h>

int main()
{   int j,n;
    printf("Input the number:");
    scanf("%d", &n);
    printf("\n");

    for(j=1; j<=n; j++)
    {
        printf("%d*%d=%d\n",n,j,n*j);
    }
    return 0;
}
